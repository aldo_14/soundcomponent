# OpenAL Sound Component

Example implementation intended for Games writing (based on LWJGL); code to load and 
play audio using OpenAL, based on receipt of events through a Guava eventBus

# Basic usage

See /run `example.ExampleApplication` in test src for an example usage.

The basic approach is simple; 
- You create an `AudioManager` using the `AudioManagerBuilder`
- Add files to the manager as key:filename (path) entries
- Pass the manager into a `EventBusListeningMusicController` object; this also takes a (guava) 
`EventBus` instance, which it will subscribe to.
- Invoke sound-playing behaviour by posting events to the bus - create events (giving a specific sound ID as per your 
earlier loading) using the `EventFactory`.
- If you want to play a sound you need to ensure it is added into the `AudioManager`; your loading strategy in this 
(i.e. whether to load in advance or dynamically) is up to you
- Make sure to call the destroy method on the manager to free up memory and close devices cleanly upon exit