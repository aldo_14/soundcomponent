package sound.events;

public interface PlaySound {
    String getSoundId();
    Mode getMode();

}
