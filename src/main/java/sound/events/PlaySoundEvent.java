package sound.events;

public class PlaySoundEvent implements PlaySound {
    private final Mode mode;
    private final String soundId;

    public PlaySoundEvent(final Mode mode, final String id) {
        this.mode = mode;
        this.soundId = id;
    }

    @Override
    public String getSoundId() {
        return soundId;
    }

    @Override
    public Mode getMode() {
        return mode;
    }

    @Override
    public String toString() {
        return String.format("%s event: %s", mode.name(), soundId);
    }
}
