package sound.events;

public enum Mode {playOnce, startLoop, endLoop, interrupt}
