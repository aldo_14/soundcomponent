package sound.events;

import static sound.events.Mode.*;

public class EventFactory {

    public static PlaySound playOnce(final String id) {
        return new PlaySoundEvent(playOnce, id);
    }

    public static PlaySound loop(final String id) {
        return new PlaySoundEvent(startLoop, id);
    }

    public static PlaySound stopLooping(final String id) {
        return new PlaySoundEvent(endLoop, id);
    }
    public static PlaySound interrupt(final String id) {
        return new PlaySoundEvent(interrupt, id);
    }
}
