package sound.controller;

import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sound.events.Mode;
import sound.events.PlaySoundEvent;
import sound.manager.AudioManager;
import sound.source.PlayableAudio;

import static sound.events.Mode.*;

/**
 * Class that subscribes to the guava event bus for any PlaySoundEvents.
 * </br></br>
 * Must be instantiated with an AudioManager object (i.e. that holds the actual PlayableAudio objects)
 * </br></br>
 * Add to the event bus with below;
 * <code>
 *     EventBusListeningMusicController listener = new EventBusListeningMusicController(...audioManager instance...);
 *      eventBus.register(listener);
 * </code>
 */
public class EventBusListeningMusicController {
        private static final Logger LOGGER = LoggerFactory.getLogger(EventBusListeningMusicController.class);

        private final AudioManager manager;

        public EventBusListeningMusicController(final AudioManager audioManager) {
                this.manager = audioManager;
        }

        @Subscribe
        public void handlePlaySoundEvent(final PlaySoundEvent event) {
                var audioFile = manager.getAudioFile(event.getSoundId());

                if(audioFile.isPresent()) {
                        handleEvent(audioFile.get(), event.getMode());
                }
                else {
                        LOGGER.warn("Unable to get audio file for {}", event);
                }
        }

        private void handleEvent(final PlayableAudio audioFile, final Mode mode) {
                if(mode.equals(playOnce)) {
                        audioFile.playOnce();
                }
                else if(mode.equals(startLoop)) {
                        audioFile.playLoop();
                }
                else if(mode.equals(endLoop)) {
                        audioFile.stopLoop();
                }
                else if(mode.equals(interrupt)) {
                        audioFile.interrupt();
                }
        }

}
