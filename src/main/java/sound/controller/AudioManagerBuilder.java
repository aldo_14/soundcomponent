package sound.controller;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import sound.manager.AudioManager;
import sound.manager.OpenAlAudioManager;

import java.util.LinkedList;
import java.util.List;

public class AudioManagerBuilder {
    private final List<Pair<String, String>> files;

    public AudioManagerBuilder() {
        files = new LinkedList<>();
    }

    public AudioManagerBuilder withFile(String id, String file) {
        files.add(new ImmutablePair<>(id, file));
        return this;
    }

    public AudioManager build() {
        final var manager = new OpenAlAudioManager();
        files.forEach(entry -> manager.loadAudioFile(entry.getKey(), entry.getValue()));
        return manager;
    }
}
