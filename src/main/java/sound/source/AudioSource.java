package sound.source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.ShortBuffer;
import java.nio.file.Files;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.stb.STBVorbis.stb_vorbis_decode_filename;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.libc.LibCStdlib.free;

/**
 * Class holding buffered audio data and providing functionality for playing it
 */
public class AudioSource implements PlayableAudio {
    private static final Logger LOGGER = LoggerFactory.getLogger(AudioSource.class);
    private final int bufferPointer;
    private final int sourcePointer;

    public static PlayableAudio fromFile(final String file) {
        if(!Files.isRegularFile(new File(file).toPath())) {
            LOGGER.error("Tried to load file that doesn't exist: {}", file);
            throw new RuntimeException(file + " does not exist");
        }
        else {
            LOGGER.info("Loading file {}", file);
        }
        return new AudioSource(file);
    }

    private AudioSource(final String audioFile) {
        stackPush();
        final var channelsBuffer = stackMallocInt(1);
        stackPush();
        final var sampleRateBuffer = stackMallocInt(1);
        final var rawAudioBuffer = stb_vorbis_decode_filename(audioFile, channelsBuffer, sampleRateBuffer);
        final var channels =  channelsBuffer.get();
        final var samples = sampleRateBuffer.get();
        stackPop();
        stackPop();
        this.bufferPointer = getBufferPointer(rawAudioBuffer, channels, samples);
        this.sourcePointer = alGenSources();
    }

    private int getBufferPointer(final ShortBuffer rawAudioBuffer, final int channels, final int sampleRate) {
        final var bufferPointer = alGenBuffers();
        alBufferData(bufferPointer, getFormat(channels), rawAudioBuffer, sampleRate);
        free(rawAudioBuffer);
        return bufferPointer;
    }

    private int getFormat(final int channels) {
        return  channels == 1 ?  AL_FORMAT_MONO16 :
                (channels == 2?  AL_FORMAT_STEREO16 : -1);
    }

    @Override
    public void playOnce() {
        stopLoop();
        alSourcei(sourcePointer, AL_BUFFER, this.bufferPointer);
        alSourcePlay(sourcePointer);
    }

    @Override
    public void interrupt() {
        alSourcei(sourcePointer, AL_BUFFER, this.bufferPointer);
        alSourceStop(sourcePointer);
    }

    @Override
    public void playLoop() {
        stopLoop();
        alSourcei(sourcePointer, AL_LOOPING, 1);
        alSourcei(sourcePointer, AL_BUFFER, this.bufferPointer);
        alSourcePlay(sourcePointer);
    }

    @Override
    public void stopLoop() {
        alSourcei(sourcePointer, AL_LOOPING, 0);
        alSourcei(sourcePointer, AL_BUFFER, this.bufferPointer);
        alSourcePlay(sourcePointer);
    }

    @Override
    public void destroy() {
        alDeleteBuffers(this.bufferPointer);
        alDeleteSources(this.sourcePointer);
    }
}
