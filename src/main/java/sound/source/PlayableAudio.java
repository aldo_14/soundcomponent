package sound.source;


public interface PlayableAudio {
    void playOnce();
    void interrupt();
    void playLoop();
    void stopLoop();
    void destroy();
}
