package sound.manager;

import sound.source.PlayableAudio;

import java.util.Optional;

public interface AudioManager {
    void loadAudioFile(String id, String filename);

    Optional<PlayableAudio> getAudioFile(String id);

    void destroy();
}
