package sound.manager;

import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import sound.source.AudioSource;
import sound.source.PlayableAudio;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.lwjgl.openal.ALC10.*;

public class OpenAlAudioManager implements AudioManager {
    private final Map<String, PlayableAudio> audio;
    private final long device;
    private final long context;

    public OpenAlAudioManager() {
        audio = new HashMap<>();
        device = alcOpenDevice(alcGetString(0, ALC_DEFAULT_DEVICE_SPECIFIER));
        context = alcCreateContext(device, new int[]{0});
        alcMakeContextCurrent(context);
        AL.createCapabilities(ALC.createCapabilities(device));
    }

    @Override
    public void loadAudioFile(final String id, final String filename) {
        if (audio.containsKey(id)) {
            audio.get(id).destroy();
        }
        audio.put(id, AudioSource.fromFile(filename));
    }

    @Override
    public Optional<PlayableAudio> getAudioFile(final String id) {
        return Optional.ofNullable(audio.get(id));
    }

    @Override
    public void destroy() {
        audio.values().forEach(a -> a.destroy());
        alcDestroyContext(context);
        alcCloseDevice(device);
        ALC.destroy();
    }
}