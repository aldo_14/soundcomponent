package example;

import com.google.common.eventbus.EventBus;
import example.ui.ButtonClickDialog;
import sound.controller.AudioManagerBuilder;
import sound.controller.EventBusListeningMusicController;

import java.io.File;

import static java.lang.Thread.currentThread;

/**
 * This is a sample implementation that creates an event bus, loads a sample audio file, and allows user input to
 * trigger audio playing.  For obvious reasons it's not a typical JUnit test...
 */
public class ExampleApplication {

    public static void main(String[] args) throws Exception{
        var bus = new EventBus();
        var resource1 = new File(currentThread().getContextClassLoader().getResource("test.ogg").getPath());
        var resource2 = new File(currentThread().getContextClassLoader().getResource("test2.ogg").getPath());
        var manager = new AudioManagerBuilder()
                .withFile("test1", resource1.getAbsolutePath())
                .withFile("test2", resource2.getAbsolutePath())
                .build();
        var listener = new EventBusListeningMusicController(manager);
        bus.register(listener);
        new ButtonClickDialog(bus, "test1", "test2"); //NOTE; proper impl should call manager.destroy() on exit
    }
}
