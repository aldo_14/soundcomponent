package example.ui;

import com.google.common.eventbus.EventBus;
import sound.events.EventFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ButtonClickDialog {
    public ButtonClickDialog(final EventBus bus, final String... ids) {
        var dialog = createDialog(bus, ids);
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                for(String id: ids) {
                    bus.post(EventFactory.interrupt(id));
                }
                System.exit(0);
            }
        });
        dialog.setVisible(true);
    }

    private JFrame createDialog(final EventBus bus, final String... ids) {
        final var dialog = new JFrame("Audio Test");
        dialog.setLayout(new FlowLayout());
        for(String id: ids) {
            dialog.getContentPane().add(clickMeToPlay(bus, id));
        }
        dialog.setLocation(200,200);
        dialog.pack();
        return dialog;
    }

    private JButton clickMeToPlay(final EventBus bus, final String id) {
        final var button = new JButton("Click to play " + id);
        button.addActionListener( e -> bus.post(EventFactory.playOnce(id)) );
        button.setVisible(true);
        return button;
    }
}
